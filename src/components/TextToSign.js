import React from 'react';

//splits the text-input into list of characters, then creates images for each char, then returns array of img. 
const TextToSign = text => {
    console.log(text);
    const chars = text.textinput.toLowerCase().split('');
    //filters out all input that is non letters a-z
    const signs = chars.filter(char => /[a-z]/.test(char)).map((char, index) => (
        <img src={require(`../resources/signs/${char}.png`)} alt={char} key={index} />
    ));

    return signs;
}

export default TextToSign;