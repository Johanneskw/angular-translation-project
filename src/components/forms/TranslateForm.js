import React, {useState} from 'react';
import TextToSign from '../TextToSign';
import {setHistory} from '../common/storage';
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import './TranslateForm.css'

const TranslateForm = () => {
    
    const [text, updateText] = useState('');

    //updates state
    const textInputChanged = ev => {
        updateText(ev.target.value);
    };

    //adds translation to history, and clears fields
    const submitTranslate = () => {
        setHistory(text);
        updateText('');
        const inp = document.getElementById('inputText');
        inp.value = '';

    }

    const inputstyle = {
        borderRadius: '20px',
        background: 'transparent',
        border: 'none' 
    };


    return (
        <div className='form'>
            <div className='inputbox'>

                <InputGroup className="mb-3" >
                    <FormControl className='inputgr'
                        style={inputstyle}
                        placeholder="Text to translate"
                        aria-label="Text to translate"
                        aria-describedby="inputText"
                        onChange={ textInputChanged }
                        id='inputText'
                    />
                    <InputGroup.Append>
                        <InputGroup.Text style={inputstyle} onClick={ submitTranslate }><img src={require('../../resources/div/right-arrow.png')} alt="next"/></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
            <div className='signs' id='signs'>
                <TextToSign textinput = {text} />
            </div>
            <div className='signs-footer'></div>
        </div> 
    );
}

export default TranslateForm;

