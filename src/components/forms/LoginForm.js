import React, { useState } from 'react';
import { useDispatch} from 'react-redux';
import { addUser } from '../common/storage';
import { setUsernameAction } from '../store/actions/username.actions'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import './LoginForm.css';

const LoginForm = props => {

    const [ username, setUsername ] = useState('');
    
    const dispatch = useDispatch();


    const onLoginClicked = () => {
        //Check if username is given
        if (username.length <= 0) {
            alert('Insert username!');
            return;
        }
        //set username to redux state
        dispatch(setUsernameAction(username))
        addUser(username);
        props.click();
    };

    //sets username to localstorage
    const onUsernameChanged = ev => setUsername(ev.target.value);

    const inptstyle = {
        borderRadius: '20px',
        background: 'transparent',
        border: 'none',
    };

    return (
        <form className='login-form'>

            <div className='login-input'>

                <InputGroup className="mb-3" >
                    <FormControl
                        style={inptstyle}
                        placeholder="Enter your username"
                        aria-label="Enter your username"
                        aria-describedby="inputText"
                        onChange={ onUsernameChanged }
                        id='inputText'
                    />
                    <InputGroup.Append>
                        <InputGroup.Text style={inptstyle} onClick={ onLoginClicked }><img src={require('../../resources/div/right-arrow.png')} alt="next"/></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>
            </div>

            <div className='footer'></div>

        </form>
    )
}

export default LoginForm;