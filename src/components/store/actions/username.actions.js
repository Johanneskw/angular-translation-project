import { SET_USERNAME } from "./action.types";

export const setUsernameAction = (username = '') => ({
    type: SET_USERNAME,
    username
});
