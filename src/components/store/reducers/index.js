import { combineReducers } from 'redux';
import usernameReducer from './username.reducer';

export default combineReducers({
    username: usernameReducer
});