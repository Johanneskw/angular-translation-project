import React from 'react';
import { Redirect } from 'react-router-dom';
import { getUser } from './storage';

export const WithAuth = Component => props => {

    const auth = getUser();

    //checks if username has been iserted to allow navigation in app.
    if (auth != null) {
        return <Component {...props} />
    } 
    return <Redirect to='/' />
}
