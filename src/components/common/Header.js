import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import './Header.css';

const Header = () => { 
    //TODO: figure out how to render username when updated.

    const history = useHistory();

    const username = useSelector(state => state.username);

    const onClickUser = () => {
        history.replace('/userpage');
    }

    const onClickLogo = () => {
        history.replace('/translator');
    }

    return (
        <div className='header'>
            <div className='logo'>
                <img src={require("../../resources/div/Logo.png")} alt="logo" onClick={ onClickLogo }/>
            </div>
            <div className='slogan'>
                <h2>Lost in Translation</h2>
            </div>
            <div className='username'>
                <h3 onClick={ onClickUser }>{username}</h3>
            </div>
            <div className='userimg'>
                <img src={require("../../resources/div/user.png")} alt="user" onClick={ onClickUser }/>
            </div>
        </div>
    );
};

export default Header;