//returns the list of translationhistory. 
export const getHistory = () => {
    const hist = localStorage.getItem('history');
    if (hist) return JSON.parse(hist);
    return false;
}

//Handles the queue which stores the translation history
export const setHistory = textinput => {
    const hist = getHistory();
    let newhist = [textinput];
    if (!hist) {
        localStorage.setItem('history', JSON.stringify(newhist));
    } else if (hist.length === 10) {
        hist.pop();
        hist.unshift(textinput);
        localStorage.setItem('history', JSON.stringify(hist));
    } else if (hist.length < 10) {
        hist.unshift(textinput);
        localStorage.setItem('history', JSON.stringify(hist));
    }
}

export const deleteHistory = () => {
    localStorage.removeItem('history');
}

export const addUser = (username) => {
    localStorage.setItem('userName', username);
}

export const getUser = () => {
    return localStorage.getItem('userName');
}

export const removeUser = () => {
    localStorage.removeItem('userName');
}