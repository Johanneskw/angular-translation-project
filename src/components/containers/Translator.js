import React from 'react';
import { WithAuth } from '../common/WithAuth'
import TranslateForm from '../forms/TranslateForm';

const Translator = () => {  
     
    return (
        <div>
            <TranslateForm />

        </div>
    )
}

export default WithAuth(Translator);