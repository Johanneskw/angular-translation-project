import React from 'react';
import { useHistory } from 'react-router-dom';
import LoginForm from '../forms/LoginForm';
import './Login.css'

const Login = () => {

    const history = useHistory();

    //routes to translator 
    const handleLoginClicked = () => {
        history.replace('/translator');
    };

    return ( 
    <div className='login-container'>
        <div className='welcome'>
            <div className='image'>
                <img src={require("../../resources/div/Logo-Hello.png")} alt="logo-hello"/>
            </div>
            <div className='message'>
                <h1>Lost in Translation</h1>
                <h4>Get started</h4>
            </div>
        </div>
        

        <LoginForm click={handleLoginClicked} />
    </div>
    )
};
export default Login;