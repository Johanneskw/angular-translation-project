import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => {

    return (
        <div style={{textAlign:'center'}}>
            <h1>404 NOT FOUND </h1>
            <Link to='/login'>Home</Link>
            <br/>
            <img src={require('../../resources/div/error.jpg')} alt='404 error'/>
        </div>
    )
}

export default NotFound;