import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getHistory, removeUser } from '../common/storage';
import { WithAuth } from '../common/WithAuth';
import HistoryGallery from '../HistoryGallery';
import { setUsernameAction } from '../store/actions/username.actions';

const UserPage = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    //removes the user from local, and erases from redux-state. then navigates to login.
    const logOut = () => {
        removeUser();
        dispatch(setUsernameAction(''));
        history.replace('/');
    }

    return (
        <div className='userpage-container'>
            <div className='gallery-container'>
                <HistoryGallery transhist = {getHistory()} clickLogOut = {logOut}/>
            </div>
        </div>
    );
}; 

export default WithAuth(UserPage);