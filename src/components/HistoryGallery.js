import React, { useState } from 'react';
import { deleteHistory } from './common/storage';
import TextToSign from './TextToSign';
import './HistoryGallery.css';
import { Button } from 'react-bootstrap';

//Takes the history as parameter, creates an listitem for each of the items in history, and gets the signs for the text.
const HistoryGallery = props => {
    
    const [hasData, setHasData] = useState(true);

    //Deletes data from local-storage;
    const removeData = () => {
        deleteHistory();
        setHasData(false);
    }

    const logOut = () => {
        removeData();
        props.clickLogOut();
    }

    //styling for buttons
    const buttonstyle = {
        widht: '300px',
        color: 'white',
        backgroundColor: '#845EC2'
    }

    const buttons = (
        <div className='btns-box'>
                <Button variant="secondary" style={buttonstyle} onClick={removeData} id='btn-remove'>Remove data</Button>
                <Button variant="secondary" style={buttonstyle} onClick={logOut} id='btn-logout'>Log out</Button>
        </div>
    );

    //if there is no data, return only buttons. 
    if (!hasData || !props.transhist) return buttons; 
    
    //creates gallery of previous translations.
    const translations = props.transhist.map((trans, index) => {
        return (
            <div className='gallery' key={index}>
                <div className='s-box'>
                    <TextToSign textinput = {trans} />
                </div>
                <div className='f-box'>
                    <h4>{trans}</h4>
                </div>
            </div>
        )
    })

    //else return the gallery of previous translations aswll.
    return (
        <div className='gallery-class'>
            {buttons}
            {translations}
        </div>
    );   
};

export default HistoryGallery;