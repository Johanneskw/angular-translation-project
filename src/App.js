import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Header from './components/common/Header';
import Login from './components/containers/Login';
import Translator from './components/containers/Translator';
import NotFound from './components/containers/NotFound';
import UserPage from './components/containers/UserPage';


import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Route path='/translator' component={ Translator }/>
          <Route path='/userpage' component={ UserPage }/>
          <Route exact path='/' component={ Login }/>
          <Route path='*' component={ NotFound }/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
